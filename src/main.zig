const std = @import("std");
const lzw_lib = @import("lzw.zig");

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = gpa.allocator();

pub fn main() !void {
    defer {
        _ = gpa.detectLeaks();
        _ = gpa.deinit();
    }

    const stdin = std.io.getStdIn();
    const stdout = std.io.getStdOut();

    const reader = stdin.reader();
    const writer = stdout.writer();

    var encoder = lzw_lib.LzwEncode.init(allocator);
    try encoder.encode(reader, writer);
}
