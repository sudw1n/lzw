const std = @import("std");

pub const LzwEncode = struct {
    allocator: std.mem.Allocator,

    const MAGIC_1: u8 = 0x1f;
    const MAGIC_2: u8 = 0x9d;
    // reset == 1 --> 1 << 7 --> 0x80
    const MODE: u8 = 0x80;
    const MAX_BITS: u5 = 16;

    const Self = @This();

    pub fn init(allocator: std.mem.Allocator) Self {
        return LzwEncode{
            .allocator = allocator,
        };
    }

    fn init_codes(self: *Self) !std.StringArrayHashMap(u16) {
        var codes = std.StringArrayHashMap(u16).init(self.allocator);
        var next_symbol: u16 = 0;
        while (next_symbol <= 0xff) : (next_symbol += 1) {
            const str = [_]u8{
                @as(u8, @truncate(next_symbol)),
            };
            const k = try self.allocator.dupe(u8, &str);
            try codes.put(k, next_symbol);
        }
        return codes;
    }

    fn deinit_codes(self: *Self, codes: *std.StringArrayHashMap(u16)) void {
        var iter = codes.iterator();
        while (iter.next()) |entry| {
            self.allocator.free(entry.key_ptr.*);
        }
        codes.deinit();
    }

    pub fn encode(self: *Self, reader: anytype, writer: anytype) !void {
        const header = [_]u8{ MAGIC_1, MAGIC_2, (MODE | MAX_BITS) };
        try writer.writeAll(&header);

        var codes = try self.init_codes();
        defer self.deinit_codes(&codes);

        // don't include RESET symbol
        var next_symbol: u16 = 257;

        // start with 9-bit symbols
        var num_bits: u5 = 9;

        var working_str = std.ArrayList(u8).init(self.allocator);
        defer working_str.deinit();

        var bitwriter = std.io.bitWriter(.Little, writer);

        while (reader.readByte()) |c| {
            // create the augmented string and check if it's not in the symbol table
            try working_str.append(c);
            if (!codes.contains(working_str.items)) {
                // check if table is full
                if (next_symbol >= (1 << (MAX_BITS - 1))) {
                    // write RESET symbol
                    try bitwriter.writeBits(@as(u9, 0xff + 1), 9);
                    // start a new set of codes
                    self.deinit_codes(&codes);
                    codes = try self.init_codes();
                    continue;
                }
                // add the augmented string to the table
                const k = try self.allocator.dupe(u8, working_str.items);
                try codes.put(k, next_symbol);

                // use the augmented string minus `c` to get the code
                const code = codes.get(working_str.items[0 .. working_str.items.len - 1]) orelse @panic("couldn't find code for working_str");
                // write that code to output
                try bitwriter.writeBits(code, num_bits);

                // clear the working string leaving only `c`
                working_str.clearRetainingCapacity();
                try working_str.append(c);

                // update next symbol to use
                next_symbol += 1;
                // check if we need more bits
                if (next_symbol > std.math.pow(u16, 2, num_bits)) {
                    num_bits += 1;
                }
            }
        } else |err| {
            if (err != error.EndOfStream) {
                return err;
            }
        }

        // flush out the remaining codes
        if (working_str.items.len > 0) {
            const code = codes.get(working_str.items) orelse @panic("couldn't find code for working_str");
            try bitwriter.writeBits(code, num_bits);
        }

        try bitwriter.flushBits();
    }
};
